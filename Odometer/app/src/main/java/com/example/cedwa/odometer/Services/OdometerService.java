package com.example.cedwa.odometer.Services;

import android.Manifest;
import android.app.LoaderManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.ContextCompat;

import java.util.Random;

public class OdometerService extends Service {

    private LocationManager locationManager;
    private LocationListener listener;
    private static double distanceInMeters;
    private static Location lastLocation = null;
    private final IBinder binder = new OdometerBinder();
    private final Random random = new Random();
    public static final String PERMISSION_STRING = Manifest.permission.ACCESS_FINE_LOCATION;

    @Override
    public void onCreate()
    {
        super.onCreate();
        listener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {

                if(lastLocation==null)
                {
                    lastLocation=location;
                }
                distanceInMeters += location.distanceTo(lastLocation);
                lastLocation=location;

            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if(ContextCompat.checkSelfPermission(this, PERMISSION_STRING) == PackageManager.PERMISSION_GRANTED)
        {
            String provider = locationManager.getBestProvider(new Criteria(), true);
            if(provider == null)
            {
                locationManager.requestLocationUpdates(provider,1000,1,listener);
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {

        return binder;

    }

    public class OdometerBinder extends Binder
    {
        public OdometerService getOdometer()
        {
            return  OdometerService.this;
        }
    }








    public double getDistance()
    {
        return this.distanceInMeters/1609.344;
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        if(locationManager!=null && listener!=null)
        {
            if(ContextCompat.checkSelfPermission(this,PERMISSION_STRING)==PackageManager.PERMISSION_GRANTED)
            {
                locationManager.removeUpdates(listener);
            }
            locationManager=null;
            listener=null;
        }
    }

}

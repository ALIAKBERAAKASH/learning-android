#include<cstdio>
#include<iostream>
#include<string>
#include<map>
#include<sstream>
#include<vector>

using namespace std;

bool notAdded(int x, vector <int> v)
{
    bool j=true;

    for(int i=0; i<v.size()&&j; i++)
        if(v[i]==x)
            j=false;

    return j;
}

int main()
{
    vector <int> order;
    map <int,int> m;
    string str;
    int num;

    while(cin >> num)
    {
        if(notAdded(num,order))
            order.push_back(num);

        m[num]++;
    }

    for(int i=0; i<order.size(); i++)
    {
        printf("%d %d\n", order[i], m[order[i]]);
    }


    return 0;
}

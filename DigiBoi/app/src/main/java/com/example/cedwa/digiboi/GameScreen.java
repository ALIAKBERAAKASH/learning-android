package com.example.cedwa.digiboi;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;
import java.util.Random;

public class GameScreen extends AppCompatActivity {

    int length = ResourceClass.resourceString.length;
    int quesId;
    private int seconds=5;
    private int quesLeft = length;
    private boolean winning;
    private boolean wasWinning;
    private boolean[] askedQuestion = new boolean[length];
    private Button showAnswerButton;

    private RadioGroup radioGroup;
    private RadioButton options, optionA,optionB,optionC,optionD;
    TextView questionText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_screen);
        winning=true;
        showAnswerButton=findViewById(R.id.check_button);
        questionText = findViewById(R.id.qs_id);
        optionA=findViewById(R.id.optionA);
        optionB=findViewById(R.id.optionB);
        optionC=findViewById(R.id.optionC);
        optionD=findViewById(R.id.optionD);
        quesId=newQuestion();
        if(savedInstanceState!=null)
        {
            seconds=savedInstanceState.getInt("seconds");
            winning = savedInstanceState.getBoolean("winning");
            wasWinning=savedInstanceState.getBoolean("wasWinning");
        }

        runTimer();
    }

   @Override
    protected void onPause() {
        super.onPause();
        wasWinning=winning;
        winning=false;
    }

    @Override
   protected void onResume() {
        super.onResume();
        if(wasWinning)
        {
            winning=true;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("seconds", seconds);
        outState.putBoolean("winning", winning);
        outState.putBoolean("wasWinning", wasWinning);
    }

    public void runTimer()
    {
        final TextView timer = findViewById(R.id.timerId);
        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                String time = String.format(Locale.getDefault(), "%d", seconds);
                timer.setText(time);
                if(winning && seconds>0)
                {
                    seconds--;
                }
                else
                {
                    timer.setText(R.string.time_up_text);
                    timer.setTextSize(20);
                    showAnswerButton.setText(R.string.view_score);
                }

                handler.postDelayed(this, 1000);
            }

        });


    }



    public void onShowAnswer(View view)
    {
        if(seconds>0 && quesLeft>0) {



            String answerText = ResourceClass.resourceString[quesId].getAnswer();
            String givenAnswer="";

            radioGroup = findViewById(R.id.radio_group_id);


            int selectedId = radioGroup.getCheckedRadioButtonId();

            options = findViewById(selectedId);

            boolean go = true;

            try {
                givenAnswer = (String) options.getText();
            } catch (Exception e) {
                go = false;
                Toast.makeText(GameScreen.this, R.string.toast_string, Toast.LENGTH_SHORT).show();
            }

            if(go)
            {
                if (answerText==givenAnswer)
                {
                    seconds=5;
                    quesId=newQuestion();
                }
                else
                    changeIntent();
            }
        }
        else
            changeIntent();
    }

    int newQuestion()
    {
        while(quesLeft>0)
        {
            Random rand = new Random();
            int x = rand.nextInt(length);
            if(!askedQuestion[x])
            {
                quesLeft--;
                askedQuestion[x]=true;
                questionText.setText(ResourceClass.resourceString[x].getQuestion());
                optionA.setText(ResourceClass.resourceString[x].getOptionA());
                optionB.setText(ResourceClass.resourceString[x].getOptionB());
                optionC.setText(ResourceClass.resourceString[x].getOptionC());
                optionD.setText(ResourceClass.resourceString[x].getOptionD());
                return x;
            }
        }

        return  -1;
    }

    void changeIntent()
    {
        Intent intent = new Intent(this, ScoreScreen.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder
                .setTitle(R.string.title)
                .setPositiveButton(R.string.positive_button, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                })
                .setNegativeButton(R.string.negative_button, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                })
                .show();
    }


}

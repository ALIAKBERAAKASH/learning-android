package com.example.cedwa.digiboi;

public class ResourceClass {

    private String question;
    private String optionA;
    private String optionB;
    private String optionC;
    private String optionD;
    private String answer;

    private ResourceClass(String question, String optionA, String optionB, String optionC, String optionD, String answer)
    {
        this.question=question;
        this.optionA=optionA;
        this.optionB=optionB;
        this.optionC=optionC;
        this.optionD=optionD;
        this.answer=answer;
    }


    public static  final ResourceClass[] resourceString= {
            new ResourceClass("কম্পিউটার প্রোগ্রামের ভাষার সর্বনিম্ন স্তর কোনটি?", "মেশিন ভাষা",  "হাই লেভেল ভাষা", "অ্যাসেম্বলি ভাষা", "চতুর্থ প্রজন্মের ভাষা", "মেশিন ভাষা" ),
            new ResourceClass("কোন ভাষায় লিখিত প্রোগ্রাম কম্পিউটার সরাসরি বুঝতে পারে?",  "মেশিন ভাষা",  "হাই লেভেল ভাষা", "অ্যাসেম্বলি ভাষা", "চতুর্থ প্রজন্মের ভাষা", "মেশিন ভাষা"),
            new ResourceClass("কোন ভাসাহ দিয়ে কম্পিউটার এর মেমরি-এড্রেসের সঙ্গে সরাসরি যোগাযোগ করা সম্ভব?",  "মেশিন ভাষা",  "হাই লেভেল ভাষা", "অ্যাসেম্বলি ভাষা", "চতুর্থ প্রজন্মের ভাষা", "মেশিন ভাষা")
            };

    public boolean[] askedQuestions = new boolean[3];

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getOptionA() {
        return optionA;
    }

    public void setOptionA(String optionA) {
        this.optionA = optionA;
    }

    public String getOptionB() {
        return optionB;
    }

    public void setOptionB(String optionB) {
        this.optionB = optionB;
    }

    public String getOptionC() {
        return optionC;
    }

    public void setOptionC(String optionC) {
        this.optionC = optionC;
    }

    public String getOptionD() {
        return optionD;
    }

    public void setOptionD(String optionD) {
        this.optionD = optionD;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}

package com.example.cedwa.notetaker2.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.example.cedwa.notetaker2.R;

public class ExampleDialog extends AppCompatDialogFragment {

    private EditText studentName;
    private EditText regNo;
    private EditText phoneNo;
    private EditText emailAddress;
    private String title ="";
    private boolean operationType;
    long id;

    private ExampleDialogListner listner;


    public void setValues(String title, boolean operationType, long id)
    {
        //operationType is true for insert and false for update
        this.title = title;
        this.operationType = operationType;
        this.id = id;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();


        View view = inflater.inflate(R.layout.layout_dialog,null);

        builder.setView(view)
                .setTitle(title)
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        String name = studentName.getText().toString();
                        String reg = regNo.getText().toString();
                        String phone = phoneNo.getText().toString();
                        String email = emailAddress.getText().toString();

                        if(operationType)
                            listner.applyStrings(name,reg,phone,email);
                        else
                            listner.updateDatabase(id,name,reg,phone,email);

                    }
                });

        studentName = view.findViewById(R.id.student_name);
        regNo = view.findViewById(R.id.reg_no);
        phoneNo = view.findViewById(R.id.phone_no);
        emailAddress = view.findViewById(R.id.email);

        return builder.create();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listner = (ExampleDialogListner) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()+"must implement ExampleDialogListener");
        }
    }

    public interface ExampleDialogListner
    {
        void applyStrings(String name, String regNo, String phoneNo, String email);
        void updateDatabase(long id,String name, String regNo, String phoneNo, String email);
    }

}

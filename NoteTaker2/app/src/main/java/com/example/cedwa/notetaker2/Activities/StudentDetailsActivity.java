package com.example.cedwa.notetaker2.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.cedwa.notetaker2.Database.Util.Student;
import com.example.cedwa.notetaker2.R;

public class StudentDetailsActivity extends AppCompatActivity {

    private Student student;
    private TextView nameField;
    private TextView regNoField;
    private TextView phoneField;
    private TextView emailField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_details);

        Intent intent = getIntent();
        student = (Student) intent.getSerializableExtra("studentObject");

        nameField = findViewById(R.id.nameField);
        regNoField = findViewById(R.id.regField);
        phoneField = findViewById(R.id.phoneField);
        emailField = findViewById(R.id.emailField);

        nameField.setText(student.getName());
        regNoField.setText(String.valueOf(student.getRegNo()));
        phoneField.setText(student.getPhone());
        emailField.setText(student.getEmail());

    }
}

package com.example.cedwa.notetaker2.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.widget.Toast;

import com.example.cedwa.notetaker2.Database.Util.Config;
import com.example.cedwa.notetaker2.Database.Util.Student;
import com.orhanobut.logger.Logger;

import java.util.ArrayList;
import java.util.List;

public class DatabaseQueryClass {

    private Context context;

    public DatabaseQueryClass(Context context)
    {
        this.context=context;
    }

    public long insertStudent(Student student)
    {
        long id =-1;

        DatabaseHelper databaseHelper = DatabaseHelper.getInstance(context);
        SQLiteDatabase sqLiteDatabase = databaseHelper.getWritableDatabase();

        //Toast.makeText(context, student.getName()+" "+student.getRegNo()+" "+student.getPhone()+" "+student.getEmail(), Toast.LENGTH_SHORT).show();

        ContentValues contentValues = new ContentValues();
        contentValues.put(Config.COLUMN_NAME,student.getName());
        contentValues.put(Config.COLUMN_REG_NO,student.getRegNo());
        contentValues.put(Config.COLUMN_PHONE,student.getPhone());
        contentValues.put(Config.COLUMN_EMAIL,student.getEmail());

        try
        {
            id=sqLiteDatabase.insertOrThrow(Config.TABLE_STUDENT, null, contentValues);
        }
        catch (SQLiteException e)
        {
            Logger.d("Exception: "+e.getMessage());
            Toast.makeText(context, "Failed to insert data! :(", Toast.LENGTH_SHORT).show();
        }
        finally {
            sqLiteDatabase.close();
        }

        return id;
    }

    public long updateStudent(Student student)
    {
        long rowCount = 0;

        DatabaseHelper databaseHelper = DatabaseHelper.getInstance(context);
        SQLiteDatabase sqLiteDatabase = databaseHelper.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(Config.COLUMN_NAME,student.getName());
        contentValues.put(Config.COLUMN_REG_NO,student.getRegNo());
        contentValues.put(Config.COLUMN_PHONE,student.getPhone());
        contentValues.put(Config.COLUMN_EMAIL,student.getEmail());

        try
        {
            rowCount = sqLiteDatabase.update(Config.TABLE_STUDENT, contentValues, Config.COLUMN_ID+" = ? ",
                    new String[]{String.valueOf(student.getId())});
        }
        catch (SQLiteException e)
        {
            Logger.d("Exception: "+e.getMessage());
            Toast.makeText(context, "Failed to update data! :(", Toast.LENGTH_SHORT).show();
        }
        finally {
            sqLiteDatabase.close();
        }


        return rowCount;
    }

    public long deleteStudentByRegNo(long regNo)
    {
        long deletedRowCount =-1;

        DatabaseHelper databaseHelper = DatabaseHelper.getInstance(context);
        SQLiteDatabase sqLiteDatabase = databaseHelper.getWritableDatabase();



        try
        {
            //Toast.makeText(context, ""+regNo, Toast.LENGTH_SHORT).show();
            deletedRowCount=sqLiteDatabase.delete(Config.TABLE_STUDENT, Config.COLUMN_REG_NO+" = ? ",
                    new String[]{String.valueOf(regNo)});
        }
        catch (SQLiteException e)
        {
            Logger.d("Exception: "+e.getMessage());
            Toast.makeText(context, "Failed to delete data! :(", Toast.LENGTH_SHORT).show();
        }
        finally {
            sqLiteDatabase.close();
        }


        return deletedRowCount;
    }

    public Student getStudentByRegNo(long regNo)
    {
        DatabaseHelper databaseHelper = DatabaseHelper.getInstance(context);
        SQLiteDatabase sqLiteDatabase = databaseHelper.getReadableDatabase();

        Cursor cursor = null;
        Student student = null;

        try
        {
            cursor = sqLiteDatabase.query(Config.TABLE_STUDENT, null,
                    Config.COLUMN_REG_NO+" = ? ",
                    new String[]{String.valueOf(regNo)},null,null,null);

            if(cursor.moveToFirst())
            {
                long id = cursor.getLong(cursor.getColumnIndex(Config.COLUMN_ID));
                String name = cursor.getString(cursor.getColumnIndex(Config.COLUMN_NAME));
                long reg = cursor.getLong(cursor.getColumnIndex(Config.COLUMN_REG_NO));
                String phone = cursor.getString(cursor.getColumnIndex(Config.COLUMN_PHONE));
                String email = cursor.getString(cursor.getColumnIndex(Config.COLUMN_EMAIL));

                student = new Student(id,name,reg,phone,email);
            }

        }
        catch (SQLiteException e)
        {
            Logger.d("Exception: "+e.getMessage());
            Toast.makeText(context, "Could not read data! :(", Toast.LENGTH_SHORT).show();
        }
        finally {

            if(cursor!=null)
                cursor.close();
            sqLiteDatabase.close();

        }

        return student;

    }

    public List<Student> getAllStudent()
    {
        DatabaseHelper databaseHelper = DatabaseHelper.getInstance(context);
        SQLiteDatabase sqLiteDatabase = databaseHelper.getReadableDatabase();

        Cursor cursor = null;
        List<Student> studentList = new ArrayList<>();

        try
        {
            cursor = sqLiteDatabase.query(Config.TABLE_STUDENT,null,null,
                    null, null, null, null);

            if(cursor.moveToFirst())
            {
                do {

                    long id = cursor.getLong(cursor.getColumnIndex(Config.COLUMN_ID));
                    String name = cursor.getString(cursor.getColumnIndex(Config.COLUMN_NAME));
                    long reg = cursor.getLong(cursor.getColumnIndex(Config.COLUMN_REG_NO));
                    String phone = cursor.getString(cursor.getColumnIndex(Config.COLUMN_PHONE));
                    String email = cursor.getString(cursor.getColumnIndex(Config.COLUMN_EMAIL));

                    studentList.add( new Student(id,name,reg,phone,email));

                }while(cursor.moveToNext());
            }


        }
        catch (SQLiteException e)
        {
            Logger.d("Exception: "+e.getMessage());
            Toast.makeText(context, "Could not fetch data! :(", Toast.LENGTH_SHORT).show();
        }
        finally
        {
            if(cursor!=null)
                cursor.close();
            sqLiteDatabase.close();
        }

        return studentList;
    }

}


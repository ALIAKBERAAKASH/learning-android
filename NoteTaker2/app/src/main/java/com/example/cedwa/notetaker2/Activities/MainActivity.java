package com.example.cedwa.notetaker2.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cedwa.notetaker2.Adapter.NotesListAdapter;
import com.example.cedwa.notetaker2.Database.DatabaseQueryClass;
import com.example.cedwa.notetaker2.Database.Util.Config;
import com.example.cedwa.notetaker2.Database.Util.Student;
import com.example.cedwa.notetaker2.Dialog.ExampleDialog;
import com.example.cedwa.notetaker2.R;

import java.util.List;
import java.util.logging.Logger;

public class MainActivity extends AppCompatActivity implements ExampleDialog.ExampleDialogListner, NotesListAdapter.DeleteButtonListener{

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private DatabaseQueryClass queryClass = new DatabaseQueryClass(this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        populateActivity();

    }

    public void onFabClick(View view)
    {
        ExampleDialog exampleDialog = new ExampleDialog();
        exampleDialog.setValues(Config.INSERT_TITLE,true, -1);
        exampleDialog.show(getSupportFragmentManager(),"Example Dialog");
    }


    @Override
    public void applyStrings(String name, String regNo, String phoneNo, String email) {

        Student student = new Student(-1,name,Long.parseLong(regNo),phoneNo,email);
        queryClass.insertStudent(student);

        populateActivity();

    }

    @Override
    public void updateDatabase(long id,String name, String regNo, String phoneNo, String email) {
        Student student = new Student(id,name,Long.parseLong(regNo),phoneNo,email);
        queryClass.updateStudent(student);
        Toast.makeText(this, "The item has been updated", Toast.LENGTH_SHORT).show();

        populateActivity();

    }

    public void populateActivity()
    {
        List<Student> studentList = null;
        studentList = queryClass.getAllStudent();

        if(studentList!=null)
        {
            adapter = new NotesListAdapter(studentList, this);
            recyclerView.setAdapter(adapter);
        }

    }

    @Override
    public void deleteButtonClicked() {
        populateActivity();
    }

    @Override
    public void cardViewLongClicked(long id) {
        ExampleDialog exampleDialog = new ExampleDialog();
        exampleDialog.setValues(Config.UPDATE_TITLE,false, id);
        exampleDialog.show(getSupportFragmentManager(),"Example Dialog");
    }

    @Override
    public void cardViewClicked(Student student) {
        Intent intent = new Intent(this, StudentDetailsActivity.class);
        intent.putExtra("studentObject", student);
        startActivity(intent);
    }
}

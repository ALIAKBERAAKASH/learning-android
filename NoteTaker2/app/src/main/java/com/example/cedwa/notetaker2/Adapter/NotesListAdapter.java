package com.example.cedwa.notetaker2.Adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cedwa.notetaker2.Activities.MainActivity;
import com.example.cedwa.notetaker2.Activities.StudentDetailsActivity;
import com.example.cedwa.notetaker2.Database.DatabaseQueryClass;
import com.example.cedwa.notetaker2.Database.Util.Config;
import com.example.cedwa.notetaker2.Database.Util.Student;
import com.example.cedwa.notetaker2.R;

import java.util.List;

public class NotesListAdapter extends RecyclerView.Adapter<NotesListAdapter.ViewHolder> {

    private List<Student> studentList;
    Context context;
    DeleteButtonListener deleteButtonListener;

    public NotesListAdapter(List<Student> studentList, Context context) {
        this.studentList = studentList;
        this.context=context;
        try
        {
            deleteButtonListener = (DeleteButtonListener) context;

        }
        catch (ClassCastException e) {
            throw new ClassCastException(context.toString()+"must implement DeleteButtonListener");
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        CardView cv = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.note_card_layout,parent,false);

        return new ViewHolder(cv);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        CardView cardView = holder.cardView;

        TextView name = cardView.findViewById(R.id.card_name);
        TextView regNo = cardView.findViewById(R.id.card_reg);
        TextView phone = cardView.findViewById(R.id.card_phone);
        TextView email = cardView.findViewById(R.id.card_email);
        Button button = cardView.findViewById(R.id.delete_button);

        name.setText(studentList.get(position).getName());
        regNo.setText(String.valueOf(studentList.get(position).getRegNo()));
        phone.setText(studentList.get(position).getPhone());
        email.setText(studentList.get(position).getEmail());

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("Are you sure you want to delete this note?")
                        .setCancelable(false)
                        .setPositiveButton("yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                DatabaseQueryClass databaseQueryClass = new DatabaseQueryClass(context);
                                databaseQueryClass.deleteStudentByRegNo(studentList.get(position).getRegNo());
                                Toast.makeText(context, "The item has been deleted", Toast.LENGTH_SHORT).show();
                                if(deleteButtonListener != null)
                                deleteButtonListener.deleteButtonClicked();

                            }
                        })
                        .setNegativeButton("no", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();

            }
        });

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteButtonListener.cardViewClicked(studentList.get(position));
            }
        });

        cardView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                //Toast.makeText(context, "Long clicked on CardView", Toast.LENGTH_SHORT).show();
                deleteButtonListener.cardViewLongClicked(studentList.get(position).getId());
                return true;
            }
        });

    }

    @Override
    public int getItemCount() {
        return studentList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CardView cardView;

        public ViewHolder(CardView itemView) {
            super(itemView);
            cardView=itemView;
        }


    }

    public interface DeleteButtonListener
    {
        void deleteButtonClicked();
        void cardViewLongClicked(long id);
        void cardViewClicked(Student student);
    }
}

package com.example.cedwa.buttondemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    int count=0;
    private Button loginButton;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loginButton= findViewById(R.id.loginButtonId);
        textView = findViewById(R.id.textViewId);

        loginButton.setOnClickListener(new View.OnClickListener()
        {
            count++;
            @Override
            public void onClick(View view) {
                textView.setText("Button was pressed "+count+" times.\n");
            }
        });
    }
}

package com.example.cedwa.simplecalculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText text1,text2;
    ImageButton subButton, sumButton, multButton, divButton;
    TextView resText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text1 = findViewById(R.id.textBox1Id);
        text2 = findViewById(R.id.textBox2Id);

        subButton = findViewById(R.id.subButtonId);
        sumButton = findViewById(R.id.sumButtonId);
        multButton = findViewById(R.id.multButtonId);
        divButton = findViewById(R.id.divButtonId);

        resText = findViewById(R.id.resultTextViewId);

        sumButton.setOnClickListener(this);
        subButton.setOnClickListener(this);
        multButton.setOnClickListener(this);
        divButton.setOnClickListener(this);


    }


        @Override
        public void onClick (View view){

        try {
            String str1, str2;
            double num1 = 0, num2 = 0, res = 0;
            boolean error = false;
            str1 = text1.getText().toString();
            str2 = text2.getText().toString();

            num1 = Double.parseDouble(str1);
            num2 = Double.parseDouble(str2);

            if (view.getId() == R.id.sumButtonId)
                res = num1 + num2;
            else if (view.getId() == R.id.subButtonId)
                res = num1 - num2;
            else if (view.getId() == R.id.multButtonId)
                res = num1 * num2;
            else if (view.getId() == R.id.divButtonId) {
                if (num2 == 0)
                    error = true;
                else
                    res = num1 / num2;
            }

            if (error)
                resText.setText("You can not divide by zero!");
            else
                resText.setText("The answer is : " + res);
        }
        catch(Exception e)
        {
            Toast.makeText(MainActivity.this,"Please enter both numbers",Toast.LENGTH_SHORT).show();
        }
    }



}


package com.example.aliakber.alarmclock;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;


public class MyIntentService extends IntentService {

    public static final String EXTRA_MESSAGE = "message";
    public  static  final  int NOTIFICATION_ID = 999;

    public MyIntentService() {

        super("MyIntentService");

    }

    @Override
    protected void onHandleIntent(Intent intent) {

        String text = intent.getStringExtra(EXTRA_MESSAGE);
        showText(text);

    }

    public void showText(String text)
    {
        Intent intent = new Intent(this, AlarmActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, 0);
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);

        // Build notification
        // Actions are just fake
        Notification noti = new Notification.Builder(this)
                .setContentTitle(getResources().getString(R.string.break_time))
                .setContentText(getResources().getString(R.string.message))
                .setSmallIcon(R.drawable.icon)
                .setContentIntent(pIntent)
                .setSound(soundUri)
                .setAutoCancel(true)
                .build();
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        notificationManager.notify(NOTIFICATION_ID, noti);
    }


}

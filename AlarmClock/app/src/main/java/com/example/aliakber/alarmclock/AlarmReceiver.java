package com.example.aliakber.alarmclock;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        Intent intent1 = new Intent(context, MyIntentService.class);
        intent.putExtra(MyIntentService.EXTRA_MESSAGE, context.getResources().getString(R.string.message));
        context.startService(intent1);

    }

}

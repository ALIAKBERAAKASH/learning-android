package com.example.cedwa.notetaker.DatabaseClasses.Util;

public class Student {

    private String name;
    private String phone;
    private String email;
    private long regNo;
    private long id;

    public Student(long id, String name, long regNo, String phone, String email)
    {
        this.id = id;
        this.name = name;
        this.regNo = regNo;
        this.phone  = phone;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getRegNo() {
        return regNo;
    }

    public void setRegNo(long regNo) {
        this.regNo = regNo;
    }
}

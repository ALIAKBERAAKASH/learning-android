package com.example.cedwa.notetaker.Dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.example.cedwa.notetaker.R;

public class ExampleDialog extends AppCompatDialogFragment {

    private EditText studentName;
    private EditText regNo;
    private EditText phoneNo;
    private EditText emailAddress;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.layout_dialog,null);

        builder.setView(view)
                .setTitle("Enter information here")
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });

        studentName = view.findViewById(R.id.student_name);
        regNo = view.findViewById(R.id.reg_no);
        phoneNo = view.findViewById(R.id.phone_no);
        emailAddress = view.findViewById(R.id.email);

        return builder.create();

    }
}

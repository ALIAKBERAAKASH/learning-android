package com.example.cedwa.notetaker.DatabaseClasses.Util;

public class Config {

    public static final String DATABASE_NAME = "student_db";

    public static final String TABLE_STUDENT = "student";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_REG_NO = "reg_no";
    public static final String COLUMN_PHONE = "phone";
    public static final String COLUMN_EMAIL = "email";

    public static final String TITLE = "title";
    public static final String CREATE_STUDENT = "create_student";
    public static final String UPDATE_STUDENT = "update_student";

    public static final String CREATE_TABLE = "CREATE_TABLE "+TABLE_STUDENT+"("+
            COLUMN_ID+" INTEGER PRIMARY KEY AUTOINCREMENT "+
            COLUMN_NAME+"TEXT NOT NULL, "+
            COLUMN_REG_NO+"INTEGER NOT NULL UNIQUE, "+
            COLUMN_PHONE+"TEXT, "+
            COLUMN_ID+"TEXT)";

}

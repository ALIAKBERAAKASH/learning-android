package com.example.cedwa.notetaker.DatabaseClasses;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.cedwa.notetaker.DatabaseClasses.Util.Config;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static DatabaseHelper databaseHelper = null;

    //calling the super class with necessary arguments
    private DatabaseHelper(Context context) {
        super(context, Config.DATABASE_NAME, null, DATABASE_VERSION);
        Logger.addLogAdapter(new AndroidLogAdapter());
    }

    public static  synchronized DatabaseHelper getInstance(Context context)
    {
        if(databaseHelper==null)
        {
            databaseHelper = new DatabaseHelper(context);
        }

        return databaseHelper;
    }


    //creating the database on the onCreate method
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    Logger.d("Create Table with sql "+Config.CREATE_TABLE);
    sqLiteDatabase.execSQL(Config.CREATE_TABLE);
    Logger.d("DB created!");

    }


    //delete if exists and create a new table
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+Config.TABLE_STUDENT);

        onCreate(sqLiteDatabase);

    }
}

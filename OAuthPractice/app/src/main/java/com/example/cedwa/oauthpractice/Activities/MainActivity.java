package com.example.cedwa.oauthpractice.Activities;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cedwa.oauthpractice.NetworkClasses.AccessToken;
import com.example.cedwa.oauthpractice.ModelClasses.LoginService;
import com.example.cedwa.oauthpractice.NetworkClasses.ServiceGenerator;
import com.example.cedwa.oauthpractice.R;

import java.io.IOException;

import retrofit2.Call;

public class MainActivity extends AppCompatActivity {

    private final String clientId = "eece0520dc3d070ac01d";
    private final String clientSecret = "e4086a3337cb544b883473a0339346ef98f36b96";
    private final String redirectUri = "my://recieve.code.url";
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.codeText);
        Button loginButton = (Button)  findViewById(R.id.loginbutton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse(ServiceGenerator.API_BASE_URL + "?client_id=" + clientId + "&redirect_uri=" + redirectUri +
                                "&scope=user,repo,gist,notifications&state=abcd"));
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        Toast.makeText(this, "FUCK", Toast.LENGTH_SHORT).show();

        // the intent filter defined in AndroidManifest will handle the return from ACTION_VIEW intent
        Uri uri = getIntent().getData();
        if (uri != null && uri.toString().startsWith(redirectUri)) {
            // use the parameter your API exposes for the code (mostly it's "code")
            String code = uri.getQueryParameter("code");
            if (code != null) {
                textView.setText(code);
                // get access token
                LoginService loginService =
                        ServiceGenerator.createService(LoginService.class, clientId, clientSecret);
                Call<AccessToken> call = loginService.getAccessToken(clientId, clientSecret, code,"abcd");
                try {
                    AccessToken accessToken = call.execute().body();
                    Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    Toast.makeText(this, "IDK!!!", Toast.LENGTH_SHORT).show();
                }
            }
            else if (uri.getQueryParameter("error") != null) {
                // show an error message here
                Toast.makeText(this, "Code not received", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(this, "FUCK", Toast.LENGTH_SHORT).show();
            }
        }
    }

}

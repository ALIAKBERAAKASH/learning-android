package com.example.cedwa.porakhela;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView qs;
    private TextView ans;
    private Button checkButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ans = findViewById(R.id.ans_id);
        qs = findViewById(R.id.qs_id);
    }

    public void onShowAnswer(View view)
    {
        RadioGroup radioGroup = findViewById(R.id.radio_group_id);

        int selectedId = radioGroup.getCheckedRadioButtonId();

        RadioButton options = findViewById(selectedId);

        String ansText = (String) options.getText();

        ans.setText(ansText);
    }

}

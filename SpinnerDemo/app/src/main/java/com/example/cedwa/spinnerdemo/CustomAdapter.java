package com.example.cedwa.spinnerdemo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ConcurrentModificationException;

class CustomAdapter extends BaseAdapter{

    private String[] countryNames;
    private String[] population;
    private int[] flags;
    Context context;
    LayoutInflater inflater;

    public CustomAdapter(MainActivity mainActivity, String[] countryNames, String[] population, int[] flags) {
        this.countryNames=countryNames;
        this.population=population;
        this.flags=flags;
        this.context=mainActivity;
        //inflater = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return countryNames.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if(view==null)
        {
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.sampleview, null);


        }

        ImageView imageView = view.findViewById(R.id.imageViewId);
        imageView.setImageResource(flags[i]);

        TextView textView = view.findViewById(R.id.textView1Id);
        textView.setText(countryNames[i]);

        TextView textView2 = view.findViewById(R.id.textView2Id);
        textView2.setText(population[i]);


        return view;
    }
}

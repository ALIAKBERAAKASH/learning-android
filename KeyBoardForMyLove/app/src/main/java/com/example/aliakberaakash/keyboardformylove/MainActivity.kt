package com.example.aliakberaakash.keyboardformylove

import android.content.Context
import android.media.AudioManager
import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener{

    enum class MusicNotes {
        DO,RE,MI,FA,SOL,LA,TI,DO_OCT
    }

    var mediaPlayer : MediaPlayer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        do_button.setOnClickListener(this)
        re_button.setOnClickListener(this)
        mi_button.setOnClickListener(this)
        fa_button.setOnClickListener(this)
        sol_button.setOnClickListener(this)
        la_button.setOnClickListener(this)
        ti_button.setOnClickListener(this)
        do_oct_button.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        when(v?.id)
        {
            R.id.do_button ->
               playMusic(R.raw.do_note)
            R.id.re_button ->
                playMusic(R.raw.ti)
            R.id.mi_button ->
                playMusic(R.raw.mi)
            R.id.fa_button ->
                playMusic(R.raw.fa)
            R.id.sol_button ->
                playMusic(R.raw.sol)
            R.id.la_button ->
                playMusic(R.raw.la)
            R.id.ti_button ->
                playMusic(R.raw.si)
            R.id.do_oct_button ->
                playMusic(R.raw.do_oct)
        }

    }

    private fun playMusic(note: Int) {
        mediaPlayer = MediaPlayer.create(this, note)
        mediaPlayer?.start()
    }


}

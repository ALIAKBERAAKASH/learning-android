package com.example.cedwa.githubbasiclogin.NetworkClasses;

import android.util.Log;

import com.example.cedwa.githubbasiclogin.ModelClasses.User;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NetworkCall implements MyApiService{
    @Override
    public void userValidityCheck(final ResponseCallBack<String> callBack) {

        LoginService loginService =
                ServiceGenerator.createService(LoginService.class, "ALIAKBERAAKASH", "222233334454liverwort");
        Call<User> call = loginService.basicLogin();
        call.enqueue(new Callback<User >() {

            @Override
            public void onResponse(Call<User> call, Response<User> response) {

                User user;

                if (response.isSuccessful()) {
                     //user object available
                    user = response.body();
                    callBack.onSuccess("success", user);

                } else {
                    callBack.onError(new Exception("Could not access resources."));
                    // error response, no access to resource?
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                // something went completely south (like no internet connection)
                Log.d("Error", t.getMessage());
            }
        });
    }
}

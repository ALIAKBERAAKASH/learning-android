package com.example.cedwa.githubbasiclogin.NetworkClasses;

import com.example.cedwa.githubbasiclogin.ModelClasses.User;

public interface MyApiService {

    void userValidityCheck(ResponseCallBack<String> callBack);

}

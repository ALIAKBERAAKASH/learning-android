package com.example.cedwa.githubbasiclogin.NetworkClasses;

import com.example.cedwa.githubbasiclogin.ModelClasses.User;

import retrofit2.Call;
import retrofit2.http.POST;

public interface LoginService {

    @POST("/login")
    Call<User> basicLogin();
}

package com.example.cedwa.githubbasiclogin.NetworkClasses;

import com.example.cedwa.githubbasiclogin.ModelClasses.User;

public interface ResponseCallBack<T> {

    void onSuccess(T data, User data2);
    void onError(Throwable th);

}

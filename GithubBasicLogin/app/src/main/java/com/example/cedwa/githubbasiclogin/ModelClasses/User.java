package com.example.cedwa.githubbasiclogin.ModelClasses;

import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("login")
    private String login;
    @SerializedName("id")
    private  String id;
    @SerializedName("node_id")
    private String nodeId;

    public User(){}

    public String getLogin() {
        return login;
    }

    public String getId() {
        return id;
    }

    public String getNodeId() {
        return nodeId;
    }
}

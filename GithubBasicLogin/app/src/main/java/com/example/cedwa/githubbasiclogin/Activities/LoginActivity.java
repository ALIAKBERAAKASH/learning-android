package com.example.cedwa.githubbasiclogin.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cedwa.githubbasiclogin.ModelClasses.User;
import com.example.cedwa.githubbasiclogin.NetworkClasses.MyApiService;
import com.example.cedwa.githubbasiclogin.NetworkClasses.NetworkCall;
import com.example.cedwa.githubbasiclogin.NetworkClasses.ResponseCallBack;
import com.example.cedwa.githubbasiclogin.R;

public class LoginActivity extends AppCompatActivity {

   /* private TextView textView;*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //textView = findViewById(R.id.userDetails);
    }


    public void onButtonClick(View view)
    {
        MyApiService myApiService = new NetworkCall();

        User user;

        myApiService.userValidityCheck(new ResponseCallBack<String>() {
            @Override
            public void onSuccess(String data, User user) {
                Toast.makeText(LoginActivity.this, data, Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onError(Throwable th) {
                Toast.makeText(LoginActivity.this, th.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

}

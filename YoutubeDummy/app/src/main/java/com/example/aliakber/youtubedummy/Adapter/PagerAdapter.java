package com.example.aliakber.youtubedummy.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.aliakber.youtubedummy.TabFragments.ChannelFragment;
import com.example.aliakber.youtubedummy.TabFragments.LiveFragment;
import com.example.aliakber.youtubedummy.TabFragments.PlayListFragment;

public class PagerAdapter extends FragmentStatePagerAdapter {

    int mNoOfTabs;

    public PagerAdapter(FragmentManager fm, int mNoOfTabs) {
        super(fm);

        this.mNoOfTabs = mNoOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch(position)
        {
            case 0:
                return new ChannelFragment();
            case 1:
                return new PlayListFragment();
            case 2:
                return new LiveFragment();
            default:
                return  null;
        }
    }

    @Override
    public int getCount() {
        return mNoOfTabs;
    }
}

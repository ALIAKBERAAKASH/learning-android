package com.example.cedwa.gridviewdemo;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.zip.Inflater;

class CustomAdapter extends BaseAdapter {
    Context context;
    String[] countryNames;
    int[] flags;
    LayoutInflater inflater;

    public CustomAdapter(MainActivity mainActivity, String[] countryNames, int[] flags) {

        this.context=mainActivity;
        this.countryNames=countryNames;
        this.flags=flags;
    }


    @Override
    public int getCount() {
        return countryNames.length;
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if(view==null)
        {
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.sample_layout, viewGroup, false);

            ImageView imageView = view.findViewById(R.id.imageViewId);
            TextView textView  = view.findViewById(R.id.textViewId);

            imageView.setImageResource(flags[i]);
            textView.setText(countryNames[i]);
        }


        return view;
    }
}

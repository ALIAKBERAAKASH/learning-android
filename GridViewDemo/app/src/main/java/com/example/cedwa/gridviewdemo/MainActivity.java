package com.example.cedwa.gridviewdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.BaseAdapter;
import android.widget.GridView;

public class MainActivity extends AppCompatActivity {

    private GridView gridVew;
    private String[] countryNames;
    private int[] flags={R.drawable.bangladesh, R.drawable.japan, R.drawable.canada, R.drawable.switzerland, R.drawable.poland};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        countryNames = getResources().getStringArray(R.array.countryNames);
        gridVew = findViewById(R.id.gridViewID);

        CustomAdapter adapter = new CustomAdapter(this, countryNames, flags);

        gridVew.setAdapter(adapter);



    }
}

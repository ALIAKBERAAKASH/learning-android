package com.example.cedwa.starbuzz;

public class Constants {

    public static final String CREATE_TABLE_DRINK = "CREATE TABLE DRINK ( _id INTEGER PRIMARY KEY AUTOINCREMENT, "+
            "NAME TEXT, "+
            "DESCRIPTION TEXT, "+
            "IMAGE_RESOURCE_ID INTEGER);";
}

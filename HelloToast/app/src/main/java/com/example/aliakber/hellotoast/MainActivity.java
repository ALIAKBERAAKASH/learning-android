package com.example.aliakber.hellotoast;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    int count =0;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = findViewById(R.id.textView);
    }

    public void onToastButtonClicked(View view) {
        showToast();
    }

    private void showToast() {
        Toast.makeText(this, "Toast Button CLicked!", Toast.LENGTH_SHORT).show();
    }

    public void onCountButtonCLicked(View view) {
        count++;
        updateTextView();
    }

    private void updateTextView() {
        if(textView!=null)
            textView.setText(Integer.toString(count));
    }
}

package com.example.cedwa.sqlitedemo;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private TextView name;
    private TextView email;
    private SQLiteDatabase db;
    private Cursor cursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MySQLiteHelper helper = new MySQLiteHelper(this);

        try
        {
            db = helper.getReadableDatabase();
            cursor = db.query("customer", new String[] {"name", "email"}, null, null,
                    null, null, null);

            if(cursor.moveToFirst())
            {
               // cursor.moveToNext();
                //String id = cursor.getString(0);
                String nameString = cursor.getString(0);
                String emailString = cursor.getString(1);

                name = findViewById(R.id.nameId);
                name.setText(nameString);

                email = findViewById(R.id.emailId);
                email.setText(emailString);
            }

            cursor.close();
            db.close();
        }
        catch (SQLiteException e)
        {
            Toast toast = Toast.makeText(this, "Can not access Database", Toast.LENGTH_SHORT);
            toast.show();
        }

    }
}

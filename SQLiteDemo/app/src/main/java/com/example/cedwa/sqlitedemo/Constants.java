package com.example.cedwa.sqlitedemo;

public class Constants {

    public static final String CREATE_CUSTOMER_TABLE ="CREATE TABLE customer (_id INTEGER PRIMARY KEY AUTOINCREMENT," +
            " name TEXT NOT NULL," +
            " email TEXT);";
}

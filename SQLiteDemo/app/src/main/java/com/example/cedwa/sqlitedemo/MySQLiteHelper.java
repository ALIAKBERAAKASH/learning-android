package com.example.cedwa.sqlitedemo;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

public class MySQLiteHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "customer_info.db";
    private static final int DB_VERSION = 1;

    public MySQLiteHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        try
        {
            sqLiteDatabase.execSQL(Constants.CREATE_CUSTOMER_TABLE);
            insertCustomerDetails(sqLiteDatabase, "Aakash", "cedward318@gmail.com");
            insertCustomerDetails(sqLiteDatabase, "Ali Akber", "ali852609@gmail.com");
        }
        catch(SQLiteException e)
        {
            Log.d("MySQLiteHelper Class", " Error creating database " + e.getMessage());
        }

    }


    public static void insertCustomerDetails(SQLiteDatabase db, String name, String email)
    {
        ContentValues customerValues = new ContentValues();

        customerValues.put("name", name);
        customerValues.put("email", email);
        db.insert("customer", null, customerValues);
    }


    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}

﻿------------------------------------------------------------------- 
Anime Studio® Pro 11 by Smith Micro Software Inc.
------------------------------------------------------------------- 

Thank you for purchasing Anime Studio Pro 11 by Smith Micro Software, Inc.! This readme includes important information about your new software, some of which is not included in other documentation. Smith Micro Software, Inc. is committed to creating stable and functionally rich software tools.


------------------------------------------
Contents
------------------------------------------

- New Features in Anime Studio Pro 11

- The Smith Micro Commitment

- Registering Anime Studio Pro 11

- Content Distribution

- Important Information

- Known Issues

- System Requirements

- Technical Support

- Copyright & Trademark Notice


------------------------------------------
New Features in Anime Studio® Pro 11
------------------------------------------

Animated Bone Parenting [Anime Studio Pro 11 Only Feature]
You can now switch parents for your bones mid-animation and have Anime Studio keyframe the result.  Like the bone system itself, this new feature is intuitive and easy to use.  Use the Reparent Bone Tool at any time to reset the parent.  You can even unlink bones completely just by clicking off on the canvas.  Don't worry, they can always be reparented later.   This new addition will help speed up your workflow and allow for more detailed animations.  

Animated Bone Targets [Anime Studio Pro 11 Only Feature] 
Increase the productivity of your workflow by being able to change Target Bones on the fly!  Starting with a target already in place, you can switch to a target on any frame and Anime Studio will keyframe the result.  No longer are you stuck using just one target bone.  This opens up new animation possibilities and takes the bone system to the next level!  You can also now assign targets with a handy keyboard shortcut and the Reparent Bone Tool.

Animated Shape Ordering [Anime Studio Pro 11 Only Feature]
It’s always been easy to arrange multiple shapes on a Vector Layer.  All you have to do is click on a shape and hit the up or down arrow to move it in front or behind another shape.  The one restriction animators had was not being to move these positions mid-animation.  This is where Animated Shape Ordering comes in!  Let’s say you are creating a head turn and need to hide an asset a few seconds in.  No fear!  You can now perform shape ordering anywhere on the timeline and keyframes will be created for this function.

Batch Export Enhancements [Anime Studio Pro 11 Only Feature]
New features have been added to the Batch Export panel to make the process of rendering multiple files easier and quicker.  The biggest addition is the ability to create and save profiles for batches.  Let’s say you configure a batch to disable anti-aliasing along with rendering at half dimensions.  You can save that configuration and keep it for future projects.  This allows you to target and render groups of files, saving you time from having to export one file at a time with specific requirements.

Bone Flipping [Anime Studio Pro 11 Only Feature]
Flipping bones can be essential to certain rigs in Anime Studio.  In the past, you could only flip a bone once.  This would alter the position for the whole animation.  Version 11 brings a new and welcomed change to this workflow.  Now when you flip a bone mid-animation, a keyframe will be created, allowing the effect to change at any time.  This works with any bone setup you can think of: parents, targets, point binding and layer binding.  You can even rig your Smart Dials to flip bones, adding more flexibility to any workflow.

Frame by Frame [Anime Studio Pro 11 Only Feature]
Anime Studio is known for its awesome bone animation system.  This hasn’t changed.  What has changed is the ability to switch between two main animation styles at any time.  Yes, frame by frame animation is now easier in Anime Studio!  With the new Frame-by-Frame layer, you can jump into a frame by frame workspace.  By using new frame controls and onion skinning, you can create authentic frame by frame animation.  Mix and match this with the bone system for a powerful combination!

Gather Media
As longtime users know, you can import several file types into Anime Studio, such as images and audio files.  If these files are ever misplaced, assets will start breaking within your project file.  Anime Studio 11 fixes this issue by taking all files you’ve imported and puts them into a new directory.  This keeps all your assets in one spot, eliminating the need for file digging.  Additionally, Anime Studio will generate a text file listing all your assets as a reference.  Version 11 does the organizing for you!

Group with Selection Layers [Anime Studio Pro 11 Only Feature]
Have you ever created two layers with detailed assets but wished later you would have set up a Group Layer to house the artwork?  This is where the new Group with Selection Layer type comes in handy!  With Anime Studio Pro 11, when you select two or more layers at once, you can choose the Group with Selection Layer option in the Layers panel to group the layers together.  This can save you time as opposed to dragging the layers into the group.

Improved Photoshop File Support [Anime Studio Pro 11 Only Feature]
Photoshop file support has been included in past versions.  One issue was the inability to refresh new PSD layers into Anime Studio after editing the original Photoshop file.  The same applied when layer reordering would occur in Photoshop.  Now, new PSD layers will be applied into Anime Studio when the active file is updated in Photoshop.  If layers are re-arranged, Anime Studio will maintain its own layer ordering but still reflect the visual changes made from Photoshop.

Layer Referencing [Anime Studio Pro 11 Only Feature]
You can now duplicate layers that take on the properties of the original layer.  This even extends past the initial creation of the layer.  As an example, let’s say you have a rigged character.  When you create a reference of the character and then move the original character’s arm, the reference layer character will also move his arm!  If changes are made to the reference layer, it will act independently from the original, allowing for the creation of unique assets.  You can even reference layers across multiple documents, opening up new workflow possibilities for team based productions.

Library Enhancements
The Library is helpful in any project and luckily it’s now easier to use!  With a click of the button, you can access several new display options.  These range from tree hierarchies to breadcrumbs.  From there you can drill down further and select how you want thumbnails be to be displayed, giving you more control over your workspace.  Last, but not least, the new Styles category allows you to save style documents for future use.

New File Format
Anime Studio 11 utilizes a JSON-based file format with an .anime extension.  This is a more modern file format that delivers faster processing when working with files.  But don’t worry, this version is still backwards compatible.  Files created in Anime Studio 5 and later can still be opened up in version 11.  Also, MotionArtist files are now compatible with Anime Studio.  This allows for an easier cross platform workflow.  

Normalize Layer Scale [Anime Studio Pro 11 Only Feature]
Have you ever resized a layer but realized later on that the line’s width had changed?  It used to be a pain trying to rescale the width to fit with the rest of your character or scene.  This is no longer a problem with Normalize Layer Scale.  This option is instant and will take the layer you resized and restore the thickness of the lines back to their default.  This means you can spend less time trying to match your strokes and more time creating your masterpiece!

Other New Features [Anime Studio Pro 11 Only Feature]
With so many new features, it’s hard to list them all!  Longtime users will be happy to know that the default camera positions have changed to reflect the functionality of a 35mm lens.  This means no more awkward stretching with close up objects.  The Properties panel is now easier to navigate and adds a section for organizing tools.  A new preview function allows for faster renders by disabling antialiasing.  A new script allows for more shape generation and you can view your 3D axes by accessing the View menu.

Styles Enhancements [Anime Studio Pro 11 Only Feature]
In past versions, when importing or creating new Styles, they had to have different names.  This was due to how Anime Studio referenced the data.  Now, Styles are referenced by a unique ID, opening up the possibility of having multiple Styles with the same name.  The big benefit of this is not worrying about encountering issues with Styles that share names during import.  You can now grab what you need without fear of errors!

Smart Bone Enhancements [Anime Studio Pro 11 Only Feature]
Smart Bone dials are a big time saver when it comes time to animate.  In the past, creating a Smart Bone dial resulted in only one action being created in the Actions panel.  This meant you would have to go in and create a second action for that dial, if you needed it.  Now, when converting a bone to a Smart Bone dial, Anime Studio will create two actions that extend to each constraint on your bone, preserving the neutral position.  Working with Smart Bones has never been easier!

Timeline Enhancements [Anime Studio Pro 11 Only Feature]
With the timeline, you can now show and hide channels based on your preferences.  This can reduce timeline clutter and help you focus on specific channels for easier editing and animation.  A new button allows you to enable and disable onionskins which is a huge time saver.  Switch, layer ordering and hold keyframes are now pill shaped for easier reading of the timeline.  Finally, bezier handles are color coded to reflect the options in graph mode, also improving the readability of the timeline.

Tools & Brushes Enhancements
Select drawing tools, such as the Freehand Tool, now create more simplified shapes.  Cutting out these extra points can help remove clutter and speed up drawing.  There’s also a new smoothing option which allows you to draw round, smooth shapes, even if your initial pass isn’t perfect.  Freehand drawing is now simplified with Merge Strokes.  When turned on, this function groups all your lines into a single shape.  The Blob Brush is now more consistent and the Delete Edge Tool has a handy new shortcut.  The New Color Points tool allows you to blend stroke colors together, creating awesome results.  Finally, Brushes give you more pleasing results with Merged Alpha and Drift Angle.

Includes many other enhancements not listed here


------------------------------------------
The Smith Micro Commitment
------------------------------------------
In our efforts to fulfill our commitment to our customers, we post updaters for our products on an ongoing basis. Please visit http://anime.smithmicro.com/updates.html to check for updates.


------------------------------------------
Registering Anime Studio® 11 
------------------------------------------

Anime Studio 11 requires registration in order to receive updates and tech support. If you haven't registered your copy of Anime Studio 11 yet, please visit 
http://www.smithmicro.com/register. Please make sure you've entered all information marked as necessary, including the Anime Studio 11 serial number you received with your purchase.
If online registration is not working or if you do not have an Internet connection, you may register Anime Studio 11 manually via postal mail, fax, or phone. To do this, print out the Smith Micro Product Registration Form included in the Anime Studio 11 Box. 


US AND GLOBAL CUSTOMERS:

POSTAL MAIL:     Smith Micro Software, Inc. (Attention: REGISTER)
                 51 Columbia
                 Aliso Viejo, CA 92656
                 USA

FAX:             +1 (949) 362-2300


We require your serial number in order to register your copy of Anime Studio 11.  


------------------------------------------
Content Distribution
------------------------------------------

Please refer to Section 5 ("Content License") of the Anime Studio - End User License Agreement ("EULA") included with this software for more information on modifying and distributing existing Anime Studio content.


----------------------------------------------------------------
Important Information:
----------------------------------------------------------------

* Microsoft® Internet Explorer® (Windows only)
Anime Studio’s Content Library utilizes Microsoft Internet Explorer technology. Windows® Internet Explorer® 9 or later is required for the embedded Content Library in Anime Studio 11. To ensure the optimal experience and compatibility for the Content Library, the installer will disable “Local Machine Lockdown” for the Anime Studio application. This will not affect other applications outside of Anime Studio. Details about “Local Machine Lockdown” can be found at http://msdn.microsoft.com/en-us/library/ms537641(VS.85).aspx. 
You can download the latest version of Internet Explorer by going to:
http://www.microsoft.com/windows/internet-explorer/worldwide-sites.aspx

*QuickTime® (Macintosh & Windows)
You must have QuickTime installed in order to playback MP3 audio files within Anime Studio, and for importing and exporting Quicktime .mov files as well as viewing included videos. The Quicktime Installer for Windows is included on the disk in the 'Third Party Installers' folder (physical purchases only). We recommend that Mac users update to the latest version of Quicktime. You can download the latest version of Quicktime by going to: http://www.apple.com/quicktime/download/  

* Adobe® Reader® (Macintosh & Windows)
You will need to have Adobe Reader installed to view the included PDF manuals.
We have included an installer for Adobe Reader in the ‘Third Party Installers’ folder on the disk (physical purchases only). You can also download the latest version of Reader by going to:
http://get.adobe.com/reader/

* You only need the Adobe® Reader® on the Mac to view the .swf files embedded in the PDF tutorials; otherwise you can use "Preview" in the Mac. 

------------------------------------------

* Note: The Upload to SendStuffNow feature that was introduced in version 7 is no longer part of the application’s file menu. If you previously signed up for a SendStuffNow account, you can access it through the website at sendstuffnow.com  


------------------------------------------
Known Issues
------------------------------------------

Anime Studio Pro 11:

- Content Library: The library in Anime Studio requires Windows® Internet Explorer® 9 or newer (Windows only) to run. 

- Content Library: Images in the library will appear clipped in some instances, to remedy this problem, we recommend that you install Windows® Internet Explorer® 9 or newer.

- Audio and Flash export; When exporting to Flash, the audio you embed in the Flash file must already be in MP3 format. You must have QuickTime installed in order to playback MP3 audio files within Anime Studio.

- You can only have one audio track if you're planning to export a Flash movie. Because Anime Studio can't encode MP3 files, it also can't mix down multiple audio streams into a single MP3 file. If you're targeting Flash, we recommend that you mix down multiple tracks and encode as MP3 prior to import into Anime Studio.

- EPS VS. Adobe Illustrator as an import choice; We recommend Illustrator as an import file format over EPS provided that the file being imported is Illustrator 8 or earlier compatible. Only import EPS if you have an EPS file. The preferred format, if available, is SVG over both EPS and Adobe Illustrator formats.

- Poser® Scenes in Anime Studio Pro 11: Poser Scenes can be very resource intensive. Using figures with large amounts of strand-based hair in Anime Studio can consume a lot of resources. We recommend to keep strand-based hair usage to a minimum. Also, importing multiple Poser Scenes, or duplicating one scene multiple times in Anime Studio may be resource intensive. Anime Studio Pro 11 requires Poser 7 or later for importing Poser Scenes. You will need to set up the preference to point to a valid Poser installation (Poser 7 or higher).
 
- When animating a Poser scene using the Poser Parameters dialog, you must save the Anime Studio document before exporting the animation.

- 64-bit Windows does not support QuickTime. As such, there are import and export limitations with the .mov format. While the application supports the import of .mov files that use a general codec (like H.264 or AAC) by using the Windows Media Foundation library, other QuickTime specific codecs are not supported. Also, QuickTime allows for the import of audio formats like .mid, .midi, .m4p and .m4b.  If you require the use of QuickTime, then we recommend using the 32-bit Windows version of Anime Studio Pro 11.

- When exporting a QuickTime movie with alpha channel on Mac OS, the resulting movie always has a premultiplied alpha channel, regardless of the checkbox that says "Do not premultiply alpha channel."

- QuickTime on 32-bit Windows does not handle paths that contain Unicode characters. To workaround this issue, Anime Studio will copy a media file with Unicode characters to a temporary folder before it is opened. This temporary media file will be deleted when it is no longer needed. Due to this extra step, very large media files with Unicode characters in the path will take longer to import. Renaming or moving the source media to a path that has no Unicode characters will avoid this performance issue.

- If you don't want any cropping, select a preset that does not include a resolution in its name.

- On Mac, exports using the "QuickTime (PNG-AAC)" output format always ignore the alpha channel with or without a soundtrack. For movie exports that require an alpha channel, use the QuickTime "ProRes 4444" presets or the "Animation" or "PNG" codec in the "QuickTime Movie" output format.

- On Windows, exports using the "MP4 (H.264-AAC)" preset only support output resolutions up to 1080p. If you wish to export to higher resolutions, use the "AVI Movie" or "QuickTime Movie" (32-bit only) output format.

- On Windows, presets like MP4 (H.264-AAC) require hardware acceleration for the video encoder. If you are noticing issues when exporting or previewing animation, make sure you are using the latest drivers for your video card. If that doesn’t work, try unchecking “Enable export/import hardware acceleration” in the application preferences.

- Use the transform bones tool to control the movement of the gorilla's eyes in the Characters/Anime Studio 11 folder by clicking and dragging the base dot on the bone


------------------------------------------
System Requirements
------------------------------------------

Windows: 
*Windows ® 7, 8 or 8.1 
*64-bit OS required for 64-bit installation
*1.3 GHz Pentium 4 or newer, Athlon 64 or newer 
*2 GB RAM recommended
*1.6 GB (Universal) or 800 MB (32-bit) free hard disk space minimum
*OpenGL enabled GPU recommended
*1024x768 display (1280x800 recommended)
*DVD-ROM drive (physical version only)
*Internet connection for product activation required
*Windows® Internet Explorer® 9 or newer


Macintosh: 
*Macintosh ® OS X 10.8, 10.9 or 10.10 
*64-bit OS required
*1.3 GHz Intel Processor or newer
*2 GB RAM recommended
*800 MB free hard drive space minimum
*1024x768 display (1280x800 recommended)
*DVD-ROM drive (physical version only)
*Internet connection for product activation required


------------------------------------------
Technical Support
------------------------------------------

You must be registered to receive technical support. If you haven't registered your copy of Anime Studio 11 yet, please visit http://www.smithmicro.com/register.

US & GLOBAL SUPPORT:

Please visit our Web site for the latest information on registration and technical support. For up to date information on our technical support policies or for our online frequently asked questions go to: http://www.smithmicro.com/support.

Note: Smith Micro reserves the right to change its support policies at any time.


------------------------------------------
Copyright & Trademark Notice
------------------------------------------

Smith Micro Software, Anime Studio, Anime Studio Debut and Anime Studio Pro are trademarks of Smith Micro Software, Inc.  Lost Marble and Moho are trademarks of Lost Marble, Inc.  All other product names mentioned in the Software, the Documentation, or other documentation are used for identification purposes only and may be trademarks or registered trademarks of their respective companies. Registered and unregistered trademarks used herein are the exclusive property of their respective owners. QuickTime and the QuickTime logo are trademarks or registered trademarks of Apple, Inc., used under license therefrom.


Version:  20150513
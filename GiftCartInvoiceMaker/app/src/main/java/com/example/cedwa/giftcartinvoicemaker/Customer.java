package com.example.cedwa.giftcartinvoicemaker;

import java.io.Serializable;

public class Customer implements Serializable {

    private String name;
    private String address;
    private String phone;
    private int numberOfItems;

    public Customer(String name, String address, String phone, int numberOfItems)
    {
        this.name=name;
        this.address=address;
        this.phone=phone;
        this.numberOfItems=numberOfItems;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getNumberOfItems() {
        return numberOfItems;
    }

    public void setNumberOfItems(int numberOfItems) {
        this.numberOfItems = numberOfItems;
    }
}

package com.example.cedwa.giftcartinvoicemaker;

import android.content.Intent;
import android.os.Environment;
import android.renderscript.ScriptGroup;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import crl.android.pdfwriter.PDFWriter;
import crl.android.pdfwriter.PaperSize;
import crl.android.pdfwriter.StandardFonts;

public class ProductsInfo extends AppCompatActivity {

    Customer customer;

    private int numberOfItems;
    private LinearLayout mainLayout;
    private EditText[] nameField;
    private EditText[] priceField;
    private EditText[] quantityField;
    private EditText deliveryChargeField,advancedField;
    private int total,advance,deliveryCharge,totalAmount,due, unitPrice, amount, quantity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products_info);

        customer = (Customer) getIntent().getSerializableExtra("sampleObject");

        numberOfItems = customer.getNumberOfItems();
        mainLayout = findViewById(R.id.mainLayout);
        deliveryChargeField=findViewById(R.id.delivery_field);
        advancedField=findViewById(R.id.advance_field);

        nameField = new EditText[numberOfItems];
        priceField = new EditText[numberOfItems];
        quantityField = new EditText[numberOfItems];

        for(int i=0; i<numberOfItems; i++)
        {
            createNewItemField(mainLayout, i);
        }
    }


    public void createNewItemField(LinearLayout mainLayout, int n)
    {
        LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);

        TextView headlineText = new TextView(this);
        TextView itemName = new TextView(this);
        TextView itemPrice = new TextView(this);
        TextView itemQuantity = new TextView(this);

        String headline = "Item no "+(n+1)+" :";

        //setting Headline
        headlineText.setLayoutParams(lparams);
        headlineText.setText(headline);
        headlineText.setTextSize(40);
        headlineText.setBackgroundColor(getResources().getColor(R.color.myTheme));
        headlineText.setTextColor(getResources().getColor(R.color.text));
        mainLayout.addView(headlineText);

        //setting name
        itemName.setLayoutParams(lparams);
        itemName.setText(R.string.name);
        mainLayout.addView(itemName);
        nameField[n] = new EditText(ProductsInfo.this);
        nameField[n].setLayoutParams(lparams);
        nameField[n].setHint("Ex: Jewellery");
        nameField[n].setInputType(InputType.TYPE_CLASS_TEXT);
        mainLayout.addView(nameField[n]);


        //setting price
        itemPrice.setLayoutParams(lparams);
        itemPrice.setText(R.string.price);
        mainLayout.addView(itemPrice);
        priceField[n] = new EditText(ProductsInfo.this);
        priceField[n].setLayoutParams(lparams);
        priceField[n].setHint("Ex: 100");
        priceField[n].setInputType(InputType.TYPE_CLASS_NUMBER);
        mainLayout.addView(priceField[n]);

        //setting quantity
        itemQuantity.setLayoutParams(lparams);
        itemQuantity.setText(R.string.quantity);
        mainLayout.addView(itemQuantity);
        quantityField[n] = new EditText(ProductsInfo.this);
        quantityField[n].setLayoutParams(lparams);
        quantityField[n].setHint("Ex: 3");
        quantityField[n].setInputType(InputType.TYPE_CLASS_NUMBER);
        mainLayout.addView(quantityField[n]);
    }

    public void onPreviewClick(View view)
    {
        if(checkFields()) {

            total = 0;
            totalAmount = 0;
            due = 0;
            amount =0;
            deliveryCharge = Integer.parseInt(deliveryChargeField.getText().toString());
            advance = Integer.parseInt(advancedField.getText().toString());

            String date = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
            String orderId = numberOfItems + date;
            int writerPosition = 585;

            //write to  pdf
            PDFWriter writer = new PDFWriter(PaperSize.FOLIO_WIDTH, PaperSize.FOLIO_HEIGHT);
            writer.setFont(StandardFonts.SUBTYPE, StandardFonts.TIMES_BOLD, StandardFonts.WIN_ANSI_ENCODING);
            writer.addText(30, 870, 42, "Gift Cart");
            writer.addText(30, 830, 18, "Luxmibazar, Dhaka-1100");
            writer.addText(460, 800, 18, "Date: " + date);
            writer.addText(30, 800, 18, "Contact no: 01683151988");
            writer.addText(10, 790, 18, "__________________________________________________________________");
            writer.addText(30, 760, 20, "Receiver's Name : " + customer.getName());
            writer.addText(30, 730, 20, "Contact : " + customer.getPhone());
            writer.addText(30, 700, 20, "Address : " + customer.getAddress());
            writer.addText(250, 650, 24, "Order Details");
            writer.addText(30, 645, 18, "_____________________________________________________________");
            writer.addText(30, 615, 22, "Description");
            writer.addText(200, 615, 22, "Quantity");
            writer.addText(350, 615, 22, "Unit Price");
            writer.addText(500, 615, 22, "Amount");
            writer.addText(30, 610, 18, "_____________________________________________________________");
            for (int i = 0; i < numberOfItems; i++, writerPosition -= 30) {

                //getting unit price and quantity
                unitPrice = Integer.parseInt(priceField[i].getText().toString());
                quantity = Integer.parseInt(quantityField[i].getText().toString());
                total+=(unitPrice*quantity);
                //adding the items details on pdf
                writer.addText(30, writerPosition, 18, nameField[i].getText().toString());
                writer.addText(230, writerPosition, 18, quantityField[i].getText().toString());
                writer.addText(380, writerPosition, 18, priceField[i].getText().toString());
                writer.addText(530, writerPosition, 18, ""+(unitPrice*quantity));
                writer.addText(30, writerPosition - 5, 18, "_____________________________________________________________");
            }
            //calculating
            due = total-advance;
            totalAmount = due+deliveryCharge;

            writer.addText(30, writerPosition - 10, 24, "Total :");
            writer.addText(530, writerPosition - 10, 18, ""+total);
            writer.addText(30, writerPosition - 40, 24, "Advanced :");
            writer.addText(530, writerPosition - 40, 18, ""+advance);
            writer.addText(30, writerPosition - 45, 18, "_____________________________________________________________");
            writer.addText(30, writerPosition - 70, 24, "Due :");
            writer.addText(530, writerPosition - 70, 18, ""+due);
            writer.addText(30, writerPosition - 100, 24, "Delivery Charge:");
            writer.addText(530, writerPosition - 100, 18, ""+deliveryCharge);
            writer.addText(30, writerPosition - 105, 18, "_____________________________________________________________");
            writer.addText(30, writerPosition - 135, 24, "Total Amount to be paid:");
            writer.addText(530, writerPosition - 135, 18, ""+totalAmount);
            writer.addText(10, writerPosition - 165, 18, "---------------------------------------------------------------------------------------------------");
            writer.addText(30, writerPosition - 200, 42, "Gift Cart");
            writer.addText(30, writerPosition - 240, 18, "Luxmibazar, Dhaka-1100");
            writer.addText(460, writerPosition - 270, 18, "Date: " + date);
            writer.addText(30, writerPosition - 270, 18, "Contact no: 01683151988");
            writer.addText(10, writerPosition - 280, 18, "__________________________________________________________________");
            writer.addText(30, writerPosition - 310, 20, "Receiver's Name : " + customer.getName());
            writer.addText(30, writerPosition - 340, 20, "Contact : " + customer.getPhone());
            writer.addText(30, writerPosition - 370, 20, "Address : " + customer.getAddress());
            writer.addText(30, writerPosition - 420, 30, "Total Amount to be paid:");
            writer.addText(530, writerPosition - 420, 30, ""+totalAmount);
            writer.addText(30, writerPosition - 450, 18, "_____________________");
            writer.addText(30, writerPosition - 470, 18, "Receiver's Signature:");

            //save pdf
            outputToFile(customer.getName() + ".pdf", writer.asString(), "ISO-8859-1");
        }
        else
        {
            Toast.makeText(this, "Please complete all fields", Toast.LENGTH_SHORT).show();
        }
    }

    public void outputToFile(String filename, String pdfContent, String encoding)
    {
        File newFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+File.separator+filename);

        try
        {
            newFile.createNewFile();
            try
            {
                FileOutputStream pdfFile = new FileOutputStream(newFile);
                pdfFile.write(pdfContent.getBytes(encoding));
                pdfFile.close();
            }
            catch(FileNotFoundException e)
            {
                Toast.makeText(this, "File not found", Toast.LENGTH_SHORT).show();
            }
            Toast.makeText(this, "File has been Saved", Toast.LENGTH_SHORT).show();
        }
        catch(IOException e)
        {
            Toast.makeText(this, "Could not create File", Toast.LENGTH_SHORT).show();
        }

    }

    boolean checkFields()
    {

            String deliveryChargeString = deliveryChargeField.getText().toString().trim();
            String advancedFieldString = advancedField.getText().toString().trim();

            if(deliveryChargeString.isEmpty() || advancedFieldString.isEmpty())
                return false;

            else
            {
                for(int i=0; i<numberOfItems; i++)
                {
                    String nameFieldString = nameField[i].getText().toString().trim();
                    String priceFieldString = priceField[i].getText().toString().trim();
                    String quantityFieldString = quantityField[i].getText().toString().trim();

                    if(nameFieldString.isEmpty() || priceFieldString.isEmpty() || quantityFieldString.isEmpty())
                        return false;

                }
            }

        return true;
    }

}

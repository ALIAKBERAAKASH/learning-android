package com.example.cedwa.giftcartinvoicemaker;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ItemDetailsActivity extends AppCompatActivity {

    private Customer customer;
    private int numberOfItems;
    private int remainingItems=0;
    private TextView nameTextView;
    private EditText nameField;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_details);

        customer = (Customer) getIntent().getSerializableExtra("sampleObject");

        numberOfItems=customer.getNumberOfItems();

        nameTextView=findViewById(R.id.item_name_id);
        nameField=findViewById(R.id.item_name_field_id);

        remainingItems++;
        nameTextView.setText("Input item"+ remainingItems +" name");

    }

    public void onNextClick(View view)
    {
        if(remainingItems<=numberOfItems)
        {
            remainingItems++;
            nameTextView.setText("Input item"+ remainingItems +" name");
            nameField.setText("");
        }
        else
        {
            Toast.makeText(this, "Your file has been saved", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, MainMenu.class);
            startActivity(intent);
        }
    }

}

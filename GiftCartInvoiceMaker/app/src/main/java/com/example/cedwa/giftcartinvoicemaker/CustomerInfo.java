package com.example.cedwa.giftcartinvoicemaker;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class CustomerInfo extends AppCompatActivity {

    EditText customerName;
    EditText customerAddress;
    EditText customerPhone;
    EditText numberOfItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_info);

        customerName=findViewById(R.id.name_text_id);
        customerAddress=findViewById(R.id.address_text_id);
        customerPhone=findViewById(R.id.phone_text_id);
        numberOfItems=findViewById(R.id.items_text_id);

    }

    public void onNextClick(View view)
    {
        String nameString=customerName.getText().toString().trim();
        String addressString=customerAddress.getText().toString().trim();
        String phoneString=customerPhone.getText().toString().trim();
        int itemsNumber;

        if(nameString.isEmpty() ||
                addressString.isEmpty() ||
                phoneString.isEmpty() ||
                numberOfItems.getText().toString().isEmpty())
        {
            Toast.makeText(this, "Please input all fields", Toast.LENGTH_SHORT).show();
        }
        else
        {
            itemsNumber = Integer.parseInt(numberOfItems.getText().toString());

            if(itemsNumber<1 || itemsNumber>20)
            {
                //Toast.makeText(this, "Number of items must be between 1 and 20", Toast.LENGTH_SHORT).show();
                numberOfItems.setError("Number of items must be between 1 and 20");
            }
            else
            {
                Customer customer = new Customer(nameString, addressString, phoneString, itemsNumber);
                Intent intent = new Intent(CustomerInfo.this, ProductsInfo.class);
                intent.putExtra("sampleObject", customer);
                startActivity(intent);
            }
        }

    }

}

package com.example.cedwa.walkingboy;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    ImageView imageView;
    TextView textView;
    LinearLayout linearLayout;

    private int movements=0;
    private int x=0;
    private boolean running;
    private boolean wasRunning;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(savedInstanceState!=null)
        {
            movements=savedInstanceState.getInt("movements");
            running = savedInstanceState.getBoolean("running");
            wasRunning = savedInstanceState.getBoolean("wasRunning");
        }

        runTimer();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInsatanceState) {
        super.onSaveInstanceState(savedInsatanceState);
        savedInsatanceState.putInt("movements", movements);
        savedInsatanceState.putBoolean("running", running);
        savedInsatanceState.putBoolean("wasRunning", wasRunning);
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        wasRunning=running;
        running=false;
    }

    protected void onResume()
    {
        super.onResume();
        if(wasRunning)
            running=true;
    }

    public void onClickStart(View view)
    {
        running = true;
    }

    public void onClickStop(View view)
    {
        running =false;
    }

    private void runTimer()
    {
        linearLayout=findViewById(R.id.linearLayoutId);
        textView = findViewById(R.id.textViewId);
        imageView=findViewById(R.id.imageId);
        final Handler handler = new Handler();


        handler.post(new Runnable()
        {
            @Override
            public void run() {

                movements=(movements%25);

                String imageName="frame"+movements;


                int id = getResources().getIdentifier("com.example.cedwa.walkingboy:drawable/" + imageName, null, null);


                imageView.setImageResource(id);
                x=x%125;
                linearLayout.setPadding(x,0,0,0);
                textView.setText(imageName);

                if(running)
                {
                    movements++;
                    x+=5;
                }

                handler.postDelayed(this, 1);
            }
        });

    }
}

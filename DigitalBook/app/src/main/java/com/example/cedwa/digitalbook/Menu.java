package com.example.cedwa.digitalbook;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class Menu extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

       // Toolbar toolbar = findViewById(R.id.toolbar_id);
       // setSupportActionBar(toolbar);

        AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(i==4)
                {
                    Intent intent = new Intent(Menu.this, ChapterFive.class);
                    startActivity(intent);
                }

            }
        };

        ListView listView = findViewById(R.id.main_menuId);
        listView.setOnItemClickListener(itemClickListener);
    }
}

package com.example.cedwa.menudemo;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import static android.app.PendingIntent.getActivity;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();

        menuInflater.inflate(R.menu.menu_design, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId()==R.id.shareMenuId)
        {

            Intent intent = new Intent(Intent.ACTION_SEND);

            intent.setType("text/plain");

            String subject = "App by Aakash";
            String body = "Check out this awesome app!!! ";

            intent.putExtra(Intent.EXTRA_SUBJECT, subject);
            intent.putExtra(Intent.EXTRA_TEXT, body);


            startActivity(Intent.createChooser(intent, "Share with "));

        }

        if(item.getItemId()==R.id.feedbackId)
        {
            Intent intent = new Intent(Intent.ACTION_SEND);
        }

        return super.onOptionsItemSelected(item);
    }

}
